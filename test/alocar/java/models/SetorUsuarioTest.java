/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.leite
 */
public class SetorUsuarioTest {
    
    public SetorUsuarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdSetor method, of class SetorUsuario.
     */
    @Test
    public void testGetIdSetor() {
        System.out.println("Teste getIdSetor");
        SetorUsuario instance = new SetorUsuario();
        instance.setIdSetor(1);
        Integer expResult = 1;
        Integer result = instance.getIdSetor();
        assertEquals(expResult, result);

    }

    /**
     * Test of setIdSetor method, of class SetorUsuario.
     */
    @Test
    public void testSetIdSetor() {
        System.out.println("Teste setIdSetor");
        SetorUsuario instance = new SetorUsuario();
        instance.setIdSetor(1);
        Integer expResult = 1;
        Integer result = instance.getIdSetor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNomeSetor method, of class SetorUsuario.
     */
    @Test
    public void testGetNomeSetor() {
        System.out.println("Teste getNomeSetor");
        SetorUsuario instance = new SetorUsuario();
        instance.setNomeSetor("ADMINISTRATIVO");
        String expResult = "ADMINISTRATIVO";
        String result = instance.getNomeSetor();
        assertEquals(expResult, result);

    }

    /**
     * Test of setNomeSetor method, of class SetorUsuario.
     */
    @Test
    public void testSetNomeSetor() {
        System.out.println("Teste setNomeSetor");
        SetorUsuario instance = new SetorUsuario();
        instance.setNomeSetor("ADMINISTRATIVO");
        String expResult = "ADMINISTRATIVO";
        String result = instance.getNomeSetor();
        assertEquals(expResult, result);

    }

    /**
     * Test of hashCode method, of class SetorUsuario.
     */
    @Test
    public void testHashCode() {
        System.out.println("Teste hashCode");
        SetorUsuario instance = new SetorUsuario();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class SetorUsuario.
     */
    @Test
    public void testEquals() {
        System.out.println("Teste equals");
        Object object = null;
        SetorUsuario instance = new SetorUsuario();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class SetorUsuario.
     */
    @Test
    public void testToString() {
         System.out.println("Teste ToString");
        SetorUsuario instance = new SetorUsuario();
        assertThat(instance.toString(), Is.isA(String.class));
        // TODO review the generated test code and remove the default call to fail.

    }
    
}
