/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.leite
 */
public class StatusveiculoTest {
    
    public StatusveiculoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdStatusv method, of class Statusveiculo.
     */
    @Test
    public void testGetIdStatusv() {
        System.out.println("Teste getIdStatusv");
        Statusveiculo instance = new Statusveiculo();
        instance.setIdStatusv(1);
        Integer expResult = 1;
        Integer result = instance.getIdStatusv();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdStatusv method, of class Statusveiculo.
     */
    @Test
    public void testSetIdStatusv() {
        System.out.println("Teste setIdStatusv");
        Statusveiculo instance = new Statusveiculo();
        instance.setIdStatusv(1);
        Integer expResult = 1;
        Integer result = instance.getIdStatusv();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStatus method, of class Statusveiculo.
     */
    @Test
    public void testGetStatus() {
        System.out.println("Teste getStatus");
        Statusveiculo instance = new Statusveiculo();
        instance.setStatus("abc");
        String expResult = "abc";
        String result = instance.getStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of setStatus method, of class Statusveiculo.
     */
    @Test
    public void testSetStatus() {
        System.out.println("Teste setStatus");
        Statusveiculo instance = new Statusveiculo();
        instance.setStatus("abc");
        String expResult = "abc";
        String result = instance.getStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Statusveiculo.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Statusveiculo instance = new Statusveiculo();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Statusveiculo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Statusveiculo instance = new Statusveiculo();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Statusveiculo.
     */
    @Test
    public void testToString() {
        System.out.println("Teste ToString");
        Veiculo instance = new Veiculo();
        assertThat(instance.toString(), Is.isA(String.class));
    }
    
}
