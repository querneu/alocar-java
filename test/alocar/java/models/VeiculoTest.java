/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.leite
 */
public class VeiculoTest {
    
    public VeiculoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getChassi method, of class Veiculo.
     */
    
    @Test
    public void testGetIdVeiculo() {
        System.out.println("Teste getIdVeiculo");
        Veiculo instance = new Veiculo();
        Integer expResult = 0;
        instance.setId_veiculo(0);
        Integer result = instance.getId_veiculo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAno method, of class Veiculo.
     */
    @Test
    public void testSetIdVeiculo() {
        System.out.println("Teste setIdVeiculo");
        Veiculo instance = new Veiculo();
        Integer expResult = 0;
        instance.setId_veiculo(0);
        Integer result = instance.getId_veiculo();
        assertEquals(expResult, result);
    }
    @Test
    public void testGetChassi() {
        System.out.println("Teste getChassi");
        Veiculo instance = new Veiculo();
        instance.setChassi("9BW ZZZ377 VT 004251");
        String expResult = "9BW ZZZ377 VT 004251";
        String result = instance.getChassi();
        assertEquals(expResult, result);
    }

    /**
     * Test of setChassi method, of class Veiculo.
     */
    @Test
    public void testSetChassi() {
        System.out.println("Teste setChassi");
        Veiculo instance = new Veiculo();
        instance.setChassi("9BW ZZZ377 VT 004251");
        String expResult = "9BW ZZZ377 VT 004251";
        String result = instance.getChassi();
        assertEquals(expResult, result);       
    }

    /**
     * Test of getMarca method, of class Veiculo.
     */
    @Test
    public void testGetMarca() {
        System.out.println("Teste getMarca");
        Veiculo instance = new Veiculo();
        instance.setMarca("CHEVROLET");
        String expResult = "CHEVROLET";
        String result = instance.getMarca();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMarca method, of class Veiculo.
     */
    @Test
    public void testSetMarca() {
        System.out.println("Teste setMarca");
        Veiculo instance = new Veiculo();
        instance.setMarca("CHEVROLET");
        String expResult = "CHEVROLET";
        String result = instance.getMarca();
        assertEquals(expResult, result);
    }

    /**
     * Test of getModelo method, of class Veiculo.
     */
    @Test
    public void testGetModelo() {
        System.out.println("Teste getModelo");
        Veiculo instance = new Veiculo();
        instance.setModelo("Sedan");
        String expResult = "Sedan";
        String result = instance.getModelo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setModelo method, of class Veiculo.
     */
    @Test
    public void testSetModelo() {
        System.out.println("Teste setModelo");
        Veiculo instance = new Veiculo();
        instance.setModelo("Sedan");
        String expResult = "Sedan";
        String result = instance.getModelo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAno method, of class Veiculo.
     */
    @Test
    public void testGetAno() {
        System.out.println("Teste getAno");
        Veiculo instance = new Veiculo();
        instance.setAno("2019");
        String expResult = "2019";
        String result = instance.getAno();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAno method, of class Veiculo.
     */
    @Test
    public void testSetAno() {
        System.out.println("Teste setAno");
        Veiculo instance = new Veiculo();
        instance.setAno("2019");
        String expResult = "2019";
        String result = instance.getAno();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTipo method, of class Veiculo.
     */
    @Test
    public void testGetTipo() {
        System.out.println("Teste getTipo");
        Veiculo instance = new Veiculo();
        instance.setTipo("Offroad");
        String expResult = "Offroad";
        String result = instance.getTipo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTipo method, of class Veiculo.
     */
    @Test
    public void testSetTipo() {
        System.out.println("Teste setTipo");
        Veiculo instance = new Veiculo();
        instance.setTipo("Offroad");
        String expResult = "Offroad";
        String result = instance.getTipo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTransmissao method, of class Veiculo.
     */
    @Test
    public void testGetTransmissao() {
        System.out.println("Teste getTransmissao");
        Veiculo instance = new Veiculo();
        instance.setTransmissao("Automática");
        String expResult = "Automática";
        String result = instance.getTransmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTransmissao method, of class Veiculo.
     */
    @Test
    public void testSetTransmissao() {
        System.out.println("Teste setTransmissao");
        Veiculo instance = new Veiculo();
        instance.setTransmissao("Automática");
        String expResult = "Automática";
        String result = instance.getTransmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCor method, of class Veiculo.
     */
    @Test
    public void testGetCor() {
        System.out.println("Teste getCor");
        Veiculo instance = new Veiculo();
        instance.setCor("Azul");
        String expResult = "Azul";
        String result = instance.getCor();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCor method, of class Veiculo.
     */
    @Test
    public void testSetCor() {
        System.out.println("Teste setCor");
        Veiculo instance = new Veiculo();
        instance.setCor("Azul");
        String expResult = "Azul";
        String result = instance.getCor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCombustivel method, of class Veiculo.
     */
    @Test
    public void testGetCombustivel() {
        System.out.println("Teste getCombustivel");
        Veiculo instance = new Veiculo();
        instance.setCombustivel("Biodiesel");
        String expResult = "Biodiesel";
        String result = instance.getCombustivel();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCombustivel method, of class Veiculo.
     */
    @Test
    public void testSetCombustivel() {
        System.out.println("Teste setCombustivel");
        Veiculo instance = new Veiculo();
        instance.setCombustivel("Biodiesel");
        String expResult = "Biodiesel";
        String result = instance.getCombustivel();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPortas method, of class Veiculo.
     */
    @Test
    public void testGetPortas() {
        System.out.println("Teste getPortas");
        Veiculo instance = new Veiculo();
        instance.setPortas("4");
        String expResult = "4";
        String result = instance.getPortas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPortas method, of class Veiculo.
     */
    @Test
    public void testSetPortas() {
        System.out.println("Teste setPortas");
        Veiculo instance = new Veiculo();
        instance.setPortas("4");
        String expResult = "4";
        String result = instance.getPortas();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Veiculo.
     */
    @Test
    public void testHashCode() {
        System.out.println("Teste hashCode");
        Veiculo instance = new Veiculo();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testgetStatusVeiculo(){
        System.out.println("Teste getStatusVeiculo :)");
        Veiculo instance = new Veiculo();
        instance.setStatusVeiculo(1);
        Integer expResult = 1;
        Integer result = instance.getStatusVeiculo();
        assertEquals(expResult, result);
    }
    @Test
    public void testsetStatusVeiculo(){
        System.out.println("Teste setStatusVeiculo :)");
        Veiculo instance = new Veiculo();
        instance.setStatusVeiculo(1);
        Integer expResult = 1;
        Integer result = instance.getStatusVeiculo();
        assertEquals(expResult, result);
    }
    /**
     * Test of equals method, of class Veiculo.
     */
    @Test
    public void testEquals() {
        System.out.println("Teste equals");
        Object object = null;
        Veiculo instance = new Veiculo();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Veiculo.
     */
    @Test
    public void testToString() {
        System.out.println("Teste ToString");
        Veiculo instance = new Veiculo();
        assertThat(instance.toString(), Is.isA(String.class));
    }
    
}
