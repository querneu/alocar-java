/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lucas Soares
 */
public class ReservaTest {
    
    public ReservaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId_reserva method, of class Reserva.
     */
    @Test
    public void testGetId_reserva() {
        System.out.println("Teste getId_reserva");
        Reserva instance = new Reserva();
        instance.setId_reserva(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_reserva();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId_reserva method, of class Reserva.
     */
    @Test
    public void testSetId_reserva() {
        System.out.println("Teste setId_reserva");
        Reserva instance = new Reserva();
        instance.setId_reserva(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_reserva();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId_veiculo method, of class Reserva.
     */
    @Test
    public void testGetId_veiculo() {
        System.out.println("Teste getId_veiculo");
        Reserva instance = new Reserva();
        instance.setId_veiculo(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_veiculo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId_veiculo method, of class Reserva.
     */
    @Test
    public void testSetId_veiculo() {
        System.out.println("Teste setId_veiculo");
        Reserva instance = new Reserva();
        instance.setId_veiculo(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_veiculo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId_cliente method, of class Reserva.
     */
    @Test
    public void testGetId_cliente() {
        System.out.println("Teste getId_cliente");
        Reserva instance = new Reserva();
        instance.setId_cliente(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_cliente();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId_cliente method, of class Reserva.
     */
    @Test
    public void testSetId_cliente() {
        System.out.println("Teste setId_cliente");
        Reserva instance = new Reserva();
        instance.setId_cliente(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_cliente();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId_status method, of class Reserva.
     */
    @Test
    public void testGetId_status() {
        System.out.println("Teste getId_status");
        Reserva instance = new Reserva();
        instance.setId_status(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_status();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId_status method, of class Reserva.
     */
    @Test
    public void testSetId_status() {
        System.out.println("Teste setId_status");
        Reserva instance = new Reserva();
        instance.setId_status(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getId_status();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Reserva.
     */
    @Test
    public void testHashCode() {
        System.out.println("Teste hashCode");
        Reserva instance = new Reserva();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Reserva.
     */
    @Test
    public void testEquals() {
        System.out.println("Teste equals");
        Object obj = null;
        Reserva instance = new Reserva();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
