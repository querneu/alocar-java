/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.leite
 */
public class PerfilTest {
    
    public PerfilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdPerfil method, of class Perfil.
     */
    @Test
    public void testGetIdPerfil() {
        System.out.println("Teste getIdPerfil");
        Perfil instance = new Perfil();
        instance.setIdPerfil(1);
        Integer expResult = 1;
        Integer result = instance.getIdPerfil();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setIdPerfil method, of class Perfil.
     */
    @Test
    public void testSetIdPerfil() {
        System.out.println("Teste setIdPerfil");
        Perfil instance = new Perfil();
        instance.setIdPerfil(1);
        Integer expResult = 1;
        Integer result = instance.getIdPerfil();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getNomePerfil method, of class Perfil.
     */
    @Test
    public void testGetNomePerfil() {
        System.out.println("Teste getNomePerfil");
        Perfil instance = new Perfil();
        instance.setNomePerfil("ADM");
        String expResult = "ADM";
        String result = instance.getNomePerfil();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setNomePerfil method, of class Perfil.
     */
    @Test
    public void testSetNomePerfil() {
        System.out.println("Teste setNomePerfil");
        Perfil instance = new Perfil();
        instance.setNomePerfil("ADM");
        String expResult = "ADM";
        String result = instance.getNomePerfil();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of hashCode method, of class Perfil.
     */
    @Test
    public void testHashCode() {
        System.out.println("Teste hashCode");
        Perfil instance = new Perfil();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of equals method, of class Perfil.
     */
    @Test
    public void testEquals() {
        System.out.println("Teste equals");
        Object object = null;
        Perfil instance = new Perfil();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Perfil.
     */
    @Test
    public void testToString() {
        System.out.println("Teste ToString");
        Perfil instance = new Perfil();
        assertThat(instance.toString(), Is.isA(String.class));
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
