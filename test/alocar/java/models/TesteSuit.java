package alocar.java.models;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({ClienteTest.class,PerfilTest.class, SetorUsuarioTest.class,UsuarioTest.class,VeiculoTest.class, StatusveiculoTest.class, AluguelTest.class, ReservaTest.class})
public class TesteSuit{
    
}
