/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.time.Instant;
import java.util.Date;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lucas Soares
 */
public class AluguelTest {
    
    public AluguelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdAluguel method, of class Aluguel.
     */
    @Test
    public void testGetIdAluguel() {
        System.out.println("Teste getIdAluguel");
        Aluguel instance = new Aluguel();
        instance.setIdAluguel(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getIdAluguel();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdAluguel method, of class Aluguel.
     */
    @Test
    public void testSetIdAluguel() {
        System.out.println("Teste setIdAluguel");
        Aluguel instance = new Aluguel();
        instance.setIdAluguel(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getIdAluguel();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdReserva method, of class Aluguel.
     */
    @Test
    public void testGetIdReserva() {
        System.out.println("Teste getIdReserva");
        Aluguel instance = new Aluguel();
        instance.setIdReserva(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getIdReserva();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdReserva method, of class Aluguel.
     */
    @Test
    public void testSetIdReserva() {
        System.out.println("Teste setIdReserva");
        Aluguel instance = new Aluguel();
        instance.setIdReserva(Integer.MIN_VALUE);
        Integer expResult = Integer.MIN_VALUE;
        Integer result = instance.getIdReserva();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDtInicio method, of class Aluguel.
     */
    @Test
    public void testGetDtInicio() {
        System.out.println("Teste getDtInicio");
        Aluguel instance = new Aluguel();
        instance.setDtInicio(Date.from(Instant.EPOCH));
        Date expResult = Date.from(Instant.EPOCH);
        Date result = instance.getDtInicio();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDtInicio method, of class Aluguel.
     */
    @Test
    public void testSetDtInicio() {
        System.out.println("Teste setDtInicio");
        Aluguel instance = new Aluguel();
        instance.setDtInicio(Date.from(Instant.EPOCH));
        Date expResult = Date.from(Instant.EPOCH);
        Date result = instance.getDtInicio();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDtFim method, of class Aluguel.
     */
    @Test
    public void testGetDtFim() {
        System.out.println("Teste getDtFim");
        Aluguel instance = new Aluguel();
        instance.setDtFim(Date.from(Instant.EPOCH));
        Date expResult = Date.from(Instant.EPOCH);
        Date result = instance.getDtFim();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDtFim method, of class Aluguel.
     */
    @Test
    public void testSetDtFim() {
        System.out.println("Teste setDtFim");
        Aluguel instance = new Aluguel();
        instance.setDtFim(Date.from(Instant.EPOCH));
        Date expResult = Date.from(Instant.EPOCH);
        Date result = instance.getDtFim();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Aluguel.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Aluguel instance = new Aluguel();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Aluguel.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Aluguel instance = new Aluguel();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Aluguel.
     */
    @Test
    public void testToString() {
        System.out.println("Teste ToString");
        Aluguel instance = new Aluguel();
        assertThat(instance.toString(), Is.isA(String.class));
    }
    
}
