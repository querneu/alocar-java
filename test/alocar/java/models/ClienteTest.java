/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.leite
 */
public class ClienteTest {
    
    public ClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
      
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setIdCliente method, of class Cliente.
     */
    @Test
    public void testSetIdCliente() {
        System.out.println("Teste set Id Cliente");
        Integer id = 0;
        Cliente instance = new Cliente();
        instance.setIdCliente(id);
        assertEquals((Integer) 0, instance.getIdCliente());
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getIdCliente method, of class Cliente.
     */
    @Test
    public void testGetIdCliente() {
        System.out.println("Teste get Id Cliente");
        Integer id = 0;
        Cliente instance = new Cliente();
        instance.setIdCliente(id);
        assertEquals((Integer) 0, instance.getIdCliente());
    }

    /**
     * Test of getCpf method, of class Cliente.
     */
    @Test
    public void testGetCpf() {
        System.out.println("Teste getCpf");
        Cliente instance = new Cliente();
        String expResult = "06161567318";
        instance.setCpf("06161567318");
        String result = instance.getCpf();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCpf method, of class Cliente.
     */
    @Test
    public void testSetCpf() {
        System.out.println("Teste setCpf");
        Cliente instance = new Cliente();
        String expResult = "06161567318";
        instance.setCpf("06161567318");
        String result = instance.getCpf();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNomeCompleto method, of class Cliente.
     */
    @Test
    public void testGetNomeCompleto() {
        System.out.println("Teste getNome Completo");
        Cliente instance = new Cliente();
        String expResult = "Lucas Soares";
        instance.setNomeCompleto("Lucas Soares");
        String result = instance.getNomeCompleto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setNomeCompleto method, of class Cliente.
     */
    @Test
    public void testSetNomeCompleto() {
        System.out.println("Teste setNome Completo");
        Cliente instance = new Cliente();
        String expResult = "Lucas Soares";
        instance.setNomeCompleto("Lucas Soares");
        String result = instance.getNomeCompleto();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataNascimento method, of class Cliente.
     */
    @Test
    public void testGetDataNascimento() {
        System.out.println("Teste getDataNascimento");
        Cliente instance = new Cliente();
        String expResult = "23081998";
        instance.setDataNascimento("23081998");
        String result = instance.getDataNascimento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataNascimento method, of class Cliente.
     */
    @Test
    public void testSetDataNascimento() {
        System.out.println("Teste setDataNascimento");
        Cliente instance = new Cliente();
        String expResult = "23081998";
        instance.setDataNascimento("23081998");
        String result = instance.getDataNascimento();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Cliente.
     */
    @Test
    public void testGetEmail() {
         System.out.println("Teste getEmail");
        Cliente instance = new Cliente();
        String expResult = "lucasoaresleite@gmail.com";
        instance.setEmail("lucasoaresleite@gmail.com");
        String result = instance.getEmail();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setEmail method, of class Cliente.
     */
    @Test
    public void testSetEmail() {
        System.out.println("Teste getEmail");
        Cliente instance = new Cliente();
        String expResult = "lucasoaresleite@gmail.com";
        instance.setEmail("lucasoaresleite@gmail.com");
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCnh method, of class Cliente.
     */
    @Test
    public void testGetCnh() {
        System.out.println("Teste getCnh");
        Cliente instance = new Cliente();
        String expResult = "15051514";
        instance.setCnh("15051514");
        String result = instance.getCnh();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCnh method, of class Cliente.
     */
    @Test
    public void testSetCnh() {
        System.out.println("Teste setCnh");
        Cliente instance = new Cliente();
        String expResult = "123456";
        instance.setCnh("123456");
        String result = instance.getCnh();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCnpj method, of class Cliente.
     */
    @Test
    public void testGetCnpj() {
        System.out.println("Teste getCnpj");
        Cliente instance = new Cliente();
        String expResult = "123456789";
        instance.setCnpj("123456789");
        String result = instance.getCnpj();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of setCnpj method, of class Cliente.
     */
    @Test
    public void testSetCnpj() {
        System.out.println("Teste setCnpj");
        Cliente instance = new Cliente();
        String expResult = "123456789";
        instance.setCnpj("123456789");
        String result = instance.getCnpj();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNomeFantasia method, of class Cliente.
     */
    @Test
    public void testGetNomeFantasia() {
        System.out.println("Teste getNomeFantasia");
        Cliente instance = new Cliente();
        String expResult = "teste";
        instance.setNomeFantasia("teste");
        String result = instance.getNomeFantasia();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setNomeFantasia method, of class Cliente.
     */
    @Test
    public void testSetNomeFantasia() {
        System.out.println("Teste setNomeFantasia");
        Cliente instance = new Cliente();
        String expResult = "teste";
        instance.setNomeFantasia("teste");
        String result = instance.getNomeFantasia();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Cliente.
     */
    @Test
    public void testHashCode() {
        System.out.println("Teste hashCode");
        Cliente instance = new Cliente();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of equals method, of class Cliente.
     */
    @Test
    public void testEquals() {
        System.out.println("Teste equals");
        Object object = null;
        Cliente instance = new Cliente();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    
    }

    /**
     * Test of toString method, of class Cliente.
     */
    @Test
    public void testToString() {
        System.out.println("Teste ToString");
        Cliente instance = new Cliente();
        assertThat(instance.toString(), Is.isA(String.class));
    }
    
}
