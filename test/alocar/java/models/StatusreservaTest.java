/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lucas Soares
 */
public class StatusreservaTest {
    
    public StatusreservaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdStatusr method, of class Statusreserva.
     */
    @Test
    public void testGetIdStatusr() {
        System.out.println("Teste getIdStatusr");
        Statusreserva instance = new Statusreserva();
        instance.setIdStatusr(1);
        Integer expResult = 1;
        Integer result = instance.getIdStatusr();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdStatusr method, of class Statusreserva.
     */
    @Test
    public void testSetIdStatusr() {
        System.out.println("Teste setIdStatusr");
        Statusreserva instance = new Statusreserva();
        instance.setIdStatusr(1);
        Integer expResult = 1;
        Integer result = instance.getIdStatusr();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStatus method, of class Statusreserva.
     */
    @Test
    public void testGetStatus() {
        System.out.println("Teste getStatus");
        Statusreserva instance = new Statusreserva();
        instance.setStatus("abc");
        String expResult = "abc";
        String result = instance.getStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of setStatus method, of class Statusreserva.
     */
    @Test
    public void testSetStatus() {
        System.out.println("Teste setStatus");
        Statusreserva instance = new Statusreserva();
        instance.setStatus("abc");
        String expResult = "abc";
        String result = instance.getStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Statusreserva.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Statusreserva instance = new Statusreserva();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Statusreserva.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Statusreserva instance = new Statusreserva();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Statusreserva.
     */
    @Test
    public void testToString() {
         System.out.println("Teste ToString");
        Statusreserva instance = new Statusreserva();
        assertThat(instance.toString(), Is.isA(String.class));
    }
    
}
