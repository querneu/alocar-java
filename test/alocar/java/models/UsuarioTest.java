/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.leite
 */
public class UsuarioTest {
    
    public UsuarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdUsuario method, of class Usuario.
     */
    @Test
    public void testGetIdUsuario() {
        System.out.println("Teste getIdUsuario");
        Usuario instance = new Usuario();
        instance.setIdUsuario(0);
        Integer expResult = 0;
        Integer result = instance.getIdUsuario();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdUsuario method, of class Usuario.
     */
    @Test
    public void testSetIdUsuario() {
        System.out.println("Teste setIdUsuario");
        Usuario instance = new Usuario();
        instance.setIdUsuario(0);
        Integer expResult = 0;
        Integer result = instance.getIdUsuario();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNomeCompleto method, of class Usuario.
     */
    @Test
    public void testGetNomeCompleto() {
        System.out.println("Teste getNomeCompleto");
        Usuario instance = new Usuario();
        instance.setNomeCompleto("Lucas Soares");
        String expResult = "Lucas Soares";
        String result = instance.getNomeCompleto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNomeCompleto method, of class Usuario.
     */
    @Test
    public void testSetNomeCompleto() {
        System.out.println("Teste setNomeCompleto");
        Usuario instance = new Usuario();
        instance.setNomeCompleto("Lucas Soares");
        String expResult = "Lucas Soares";
        String result = instance.getNomeCompleto();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLogin method, of class Usuario.
     */
    @Test
    public void testGetLogin() {
        System.out.println("Teste getLogin");
        Usuario instance = new Usuario();
        instance.setLogin("Querneu");
        String expResult = "Querneu";
        String result = instance.getLogin();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLogin method, of class Usuario.
     */
    @Test
    public void testSetLogin() {
        System.out.println("Teste setLogin");
        Usuario instance = new Usuario();
        instance.setLogin("Querneu");
        String expResult = "Querneu";
        String result = instance.getLogin();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSenha method, of class Usuario.
     */
    @Test
    public void testGetSenha() {
        System.out.println("Teste getSenha");
        Usuario instance = new Usuario();
        instance.setSenha("123");
        String expResult = "123";
        String result = instance.getSenha();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSenha method, of class Usuario.
     */
    @Test
    public void testSetSenha() {
        System.out.println("Teste setSenha");
        Usuario instance = new Usuario();
        instance.setSenha("123");
        String expResult = "123";
        String result = instance.getSenha();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class Usuario.
     */
    @Test
    public void testGetEmail() {
        System.out.println("Teste getEmail");
        Usuario instance = new Usuario();
        instance.setEmail("lucasoaresleite@gmail.com");
        String expResult = "lucasoaresleite@gmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail method, of class Usuario.
     */
    @Test
    public void testSetEmail() {
        System.out.println("Teste setEmail");
        Usuario instance = new Usuario();
        instance.setEmail("lucasoaresleite@gmail.com");
        String expResult = "lucasoaresleite@gmail.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSetor method, of class Usuario.
     */
    @Test
    public void testGetSetor() {
        System.out.println("Teste getSetor");
        Usuario instance = new Usuario();
        instance.setSetor(1);
        Integer expResult = 1;
        Integer result = instance.getSetor();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSetor method, of class Usuario.
     */
    @Test
    public void testSetSetor() {
        System.out.println("Teste setSetor");
        Usuario instance = new Usuario();
        instance.setSetor(1);
        Integer expResult = 1;
        Integer result = instance.getSetor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPerfil method, of class Usuario.
     */
    @Test
    public void testGetPerfil() {
        System.out.println("Teste getPerfil");
        Usuario instance = new Usuario();
        instance.setPerfil(1);
        Integer expResult = 1;
        Integer result = instance.getPerfil();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPerfil method, of class Usuario.
     */
    @Test
    public void testSetPerfil() {
        System.out.println("Teste setPerfil");
        Usuario instance = new Usuario();
        instance.setPerfil(1);
        Integer expResult = 1;
        Integer result = instance.getPerfil();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Usuario.
     */
    @Test
    public void testHashCode() {
        System.out.println("Teste hashCode");
        Usuario instance = new Usuario();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Usuario.
     */
    @Test
    public void testEquals() {
        System.out.println("Teste equals");
        Object object = null;
        Usuario instance = new Usuario();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Usuario.
     */
    @Test
    public void testToString() {
        System.out.println("Teste ToString");
        Veiculo instance = new Veiculo();
        assertThat(instance.toString(), Is.isA(String.class));
    }
    
}
