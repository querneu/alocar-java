-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.6-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para alocar_java
DROP DATABASE IF EXISTS `alocar_java`;
CREATE DATABASE IF NOT EXISTS `alocar_java` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `alocar_java`;

-- Copiando estrutura para tabela alocar_java.aluguel
DROP TABLE IF EXISTS `aluguel`;
CREATE TABLE IF NOT EXISTS `aluguel` (
  `id_aluguel` int(11) NOT NULL AUTO_INCREMENT,
  `id_reserva` int(11) NOT NULL,
  `dt_inicio` datetime NOT NULL,
  `dt_fim` datetime NOT NULL,
  PRIMARY KEY (`id_aluguel`),
  KEY `fk_reserva` (`id_reserva`),
  CONSTRAINT `fk_reserva` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id_reserva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela alocar_java.aluguel: ~0 rows (aproximadamente)
DELETE FROM `aluguel`;
/*!40000 ALTER TABLE `aluguel` DISABLE KEYS */;
/*!40000 ALTER TABLE `aluguel` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(50) NOT NULL DEFAULT '',
  `nome_completo` varchar(50) NOT NULL DEFAULT '',
  `data_nascimento` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `cnh` varchar(50) NOT NULL DEFAULT '',
  `cnpj` varchar(50) DEFAULT NULL,
  `nome_fantasia` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `cpf` (`cpf`),
  UNIQUE KEY `cnh` (`cnh`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabela de clientes';

-- Copiando dados para a tabela alocar_java.cliente: ~2 rows (aproximadamente)
DELETE FROM `cliente`;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`id_cliente`, `cpf`, `nome_completo`, `data_nascimento`, `email`, `cnh`, `cnpj`, `nome_fantasia`) VALUES
	(1, '061.615.673-18', 'lucas soares leite', '23/08/1998', 'lucasoaresleite@gmail.com', '651651651', '99.999.999/9999-99', 'A. Telecom'),
	(2, '222.222.222-22', 'askjdklasjdklajskldj', '23/08/1998', 'lucasoaresleite@gmail.com', 'kajdlakdlçasjkd', '99.999.999/9999-99', 'a. telecom');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.perfil
DROP TABLE IF EXISTS `perfil`;
CREATE TABLE IF NOT EXISTS `perfil` (
  `id_perfil` int(11) NOT NULL,
  `nome_perfil` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_perfil`),
  UNIQUE KEY `NOME_PERFIL` (`nome_perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para os perfis de usuário';

-- Copiando dados para a tabela alocar_java.perfil: ~3 rows (aproximadamente)
DELETE FROM `perfil`;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`id_perfil`, `nome_perfil`) VALUES
	(3, 'ADMINISTRADOR'),
	(1, 'GERENTE'),
	(2, 'VENDEDOR');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.reservas
DROP TABLE IF EXISTS `reservas`;
CREATE TABLE IF NOT EXISTS `reservas` (
  `id_reserva` int(11) NOT NULL AUTO_INCREMENT,
  `veiculo` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `FK_VEICULO` (`veiculo`),
  KEY `FK_CLIENTE` (`cliente`),
  KEY `FK_STATUS` (`status`),
  CONSTRAINT `FK_CLIENTE` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`id_cliente`),
  CONSTRAINT `FK_STATUS` FOREIGN KEY (`status`) REFERENCES `statusreserva` (`id_statusr`),
  CONSTRAINT `FK_VEICULO` FOREIGN KEY (`veiculo`) REFERENCES `veiculo` (`id_veiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela alocar_java.reservas: ~2 rows (aproximadamente)
DELETE FROM `reservas`;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
INSERT INTO `reservas` (`id_reserva`, `veiculo`, `cliente`, `status`) VALUES
	(1, 2, 2, 2),
	(2, 2, 1, 2);
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.setorusuario
DROP TABLE IF EXISTS `setorusuario`;
CREATE TABLE IF NOT EXISTS `setorusuario` (
  `id_setor` int(11) NOT NULL AUTO_INCREMENT,
  `nome_setor` varchar(50) NOT NULL,
  PRIMARY KEY (`id_setor`),
  UNIQUE KEY `nome_setor` (`nome_setor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabela delimitante do setor de usuario';

-- Copiando dados para a tabela alocar_java.setorusuario: ~0 rows (aproximadamente)
DELETE FROM `setorusuario`;
/*!40000 ALTER TABLE `setorusuario` DISABLE KEYS */;
INSERT INTO `setorusuario` (`id_setor`, `nome_setor`) VALUES
	(1, 'VENDAS');
/*!40000 ALTER TABLE `setorusuario` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.statusreserva
DROP TABLE IF EXISTS `statusreserva`;
CREATE TABLE IF NOT EXISTS `statusreserva` (
  `id_statusr` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_statusr`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela alocar_java.statusreserva: ~4 rows (aproximadamente)
DELETE FROM `statusreserva`;
/*!40000 ALTER TABLE `statusreserva` DISABLE KEYS */;
INSERT INTO `statusreserva` (`id_statusr`, `status`) VALUES
	(1, 'APROVADO'),
	(2, 'AVALIAÇÃO'),
	(3, 'NEGADO'),
	(4, 'FINALIZADO');
/*!40000 ALTER TABLE `statusreserva` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.statusveiculo
DROP TABLE IF EXISTS `statusveiculo`;
CREATE TABLE IF NOT EXISTS `statusveiculo` (
  `id_statusv` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_statusv`),
  UNIQUE KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela alocar_java.statusveiculo: ~2 rows (aproximadamente)
DELETE FROM `statusveiculo`;
/*!40000 ALTER TABLE `statusveiculo` DISABLE KEYS */;
INSERT INTO `statusveiculo` (`id_statusv`, `status`) VALUES
	(1, 'DISPONÍVEL'),
	(2, 'INDISPONÍVEL');
/*!40000 ALTER TABLE `statusveiculo` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome_completo` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `setor` int(11) DEFAULT NULL,
  `perfil` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `fksetor` (`setor`),
  KEY `fkperfil` (`perfil`),
  CONSTRAINT `fkperfil` FOREIGN KEY (`perfil`) REFERENCES `perfil` (`id_perfil`),
  CONSTRAINT `fksetor` FOREIGN KEY (`setor`) REFERENCES `setorusuario` (`id_setor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Teabela para usuarios do sistema';

-- Copiando dados para a tabela alocar_java.usuario: ~1 rows (aproximadamente)
DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id_usuario`, `nome_completo`, `login`, `senha`, `email`, `setor`, `perfil`) VALUES
	(1, 'Lucas Soares', 'querneu', 'teste123', 'lucasoaresleite@gmail.com', 1, 2);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela alocar_java.veiculo
DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE IF NOT EXISTS `veiculo` (
  `id_veiculo` int(11) NOT NULL AUTO_INCREMENT,
  `chassi` varchar(50) NOT NULL DEFAULT '',
  `marca` varchar(50) NOT NULL DEFAULT '',
  `modelo` varchar(50) NOT NULL DEFAULT '',
  `ano` varchar(50) NOT NULL DEFAULT '',
  `tipo` varchar(50) NOT NULL DEFAULT '',
  `transmissao` varchar(50) NOT NULL DEFAULT '',
  `cor` varchar(50) NOT NULL DEFAULT '',
  `combustivel` varchar(50) NOT NULL DEFAULT '',
  `portas` varchar(50) NOT NULL DEFAULT '',
  `status_veiculo` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_veiculo`),
  UNIQUE KEY `ID` (`chassi`),
  KEY `fk_status_veiculo` (`status_veiculo`),
  CONSTRAINT `fk_status_veiculo` FOREIGN KEY (`status_veiculo`) REFERENCES `statusveiculo` (`id_statusv`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Tabela de veículo genérica derivativa';

-- Copiando dados para a tabela alocar_java.veiculo: ~0 rows (aproximadamente)
DELETE FROM `veiculo`;
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` (`id_veiculo`, `chassi`, `marca`, `modelo`, `ano`, `tipo`, `transmissao`, `cor`, `combustivel`, `portas`, `status_veiculo`) VALUES
	(2, 'asd1sa32d1', 'volks', 'a4', '2019', 'pickup', 'automatica', 'vermelho', 'diesel', '4', 1),
	(3, '4asdas', 'aas', 'sdfdsf', '2020', 'hatchet', 'manual', 'branco', 'gasosa', '2', 2);
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
