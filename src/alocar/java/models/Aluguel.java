/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lucas Soares
 */
@Entity
@Table(name = "aluguel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aluguel.findAll", query = "SELECT a FROM Aluguel a")
    , @NamedQuery(name = "Aluguel.findByIdReserva", query = "SELECT a FROM Aluguel a WHERE a.id_reserva = :id_reserva")
    , @NamedQuery(name = "Aluguel.findByIdAluguel", query = "SELECT a FROM Aluguel a WHERE a.idAluguel = :idAluguel")
    , @NamedQuery(name = "Aluguel.findByDtInicio", query = "SELECT a FROM Aluguel a WHERE a.dtInicio = :dtInicio")
    , @NamedQuery(name = "Aluguel.findByDtFim", query = "SELECT a FROM Aluguel a WHERE a.dtFim = :dtFim")})
public class Aluguel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_aluguel")
    private Integer idAluguel;
    @Basic(optional = false)
    @Column(name = "id_reserva")
    private Integer idReserva;
    @Basic(optional = false)
    @Column(name = "dt_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtInicio;
    @Basic(optional = false)
    @Column(name = "dt_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtFim;
    
    public Aluguel() {
    }

    public Aluguel(Integer idAluguel) {
        this.idAluguel = idAluguel;
    }

    
    
    public Aluguel(Integer idAluguel, Integer idReserva, Date dtInicio, Date dtFim) {
        this.idAluguel = idAluguel;
        this.idReserva = idReserva;
        this.dtInicio = dtInicio;
        this.dtFim = dtFim;
    }

    public Integer getIdAluguel() {
        return idAluguel;
    }

    public void setIdAluguel(Integer idAluguel) {
        this.idAluguel = idAluguel;
    }

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Date getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(Date dtInicio) {
        this.dtInicio = dtInicio;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAluguel != null ? idAluguel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aluguel)) {
            return false;
        }
        Aluguel other = (Aluguel) object;
        return !((this.idAluguel == null && other.idAluguel != null) || (this.idAluguel != null && !this.idAluguel.equals(other.idAluguel)));
    }

    @Override
    public String toString() {
        return "alocar.java.dao.Aluguel[ idAluguel=" + idAluguel + " ]";
    }
    
}
