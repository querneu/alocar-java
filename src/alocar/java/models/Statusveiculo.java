/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas.leite
 */
@Entity
@Table(name = "statusveiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statusveiculo.findAll", query = "SELECT s FROM Statusveiculo s")
    , @NamedQuery(name = "Statusveiculo.findByIdStatusv", query = "SELECT s FROM Statusveiculo s WHERE s.idStatusv = :idStatusv")
    , @NamedQuery(name = "Statusveiculo.findByStatus", query = "SELECT s FROM Statusveiculo s WHERE s.status = :status")})
public class Statusveiculo implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusVeiculo")
    private Collection<Veiculo> veiculoCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_statusv")
    private Integer idStatusv;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;

    public Statusveiculo() {
    }

    public Statusveiculo(Integer idStatusv) {
        this.idStatusv = idStatusv;
    }

    public Statusveiculo(Integer idStatusv, String status) {
        this.idStatusv = idStatusv;
        this.status = status;
    }

    public Integer getIdStatusv() {
        return idStatusv;
    }

    public void setIdStatusv(Integer idStatusv) {
        this.idStatusv = idStatusv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatusv != null ? idStatusv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statusveiculo)) {
            return false;
        }
        Statusveiculo other = (Statusveiculo) object;
        return !((this.idStatusv == null && other.idStatusv != null) || (this.idStatusv != null && !this.idStatusv.equals(other.idStatusv)));
    }

    @Override
    public String toString() {
        return "alocar.java.models.Statusveiculo[ idStatusv=" + idStatusv + " ]";
    }

    @XmlTransient
    public Collection<Veiculo> getVeiculoCollection() {
        return veiculoCollection;
    }

    public void setVeiculoCollection(Collection<Veiculo> veiculoCollection) {
        this.veiculoCollection = veiculoCollection;
    }
    
}
