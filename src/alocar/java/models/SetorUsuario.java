/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.leite
 */
@Entity
@Table(name = "setorusuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "setorusuario.findAll", query = "SELECT s FROM setorusuario s")
    , @NamedQuery(name = "setorusuario.findByIdSetor", query = "SELECT s FROM setorusuario s WHERE s.idSetor = :idSetor")
    , @NamedQuery(name = "setorusuario.findByNomeSetor", query = "SELECT s FROM setorusuario s WHERE s.nomeSetor = :nomeSetor")})
public class SetorUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_setor")
    private Integer idSetor;
    @Basic(optional = false)
    @Column(name = "nome_setor")
    private String nomeSetor;

    public SetorUsuario() {
    }

    public SetorUsuario(Integer idSetor) {
        this.idSetor = idSetor;
    }

    public SetorUsuario(Integer idSetor, String nomeSetor) {
        this.idSetor = idSetor;
        this.nomeSetor = nomeSetor;
    }

    public Integer getIdSetor() {
        return idSetor;
    }

    public void setIdSetor(Integer idSetor) {
        this.idSetor = idSetor;
    }

    public String getNomeSetor() {
        return nomeSetor;
    }

    public void setNomeSetor(String nomeSetor) {
        this.nomeSetor = nomeSetor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSetor != null ? idSetor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SetorUsuario)) {
            return false;
        }
        SetorUsuario other = (SetorUsuario) object;
        return !((this.idSetor == null && other.idSetor != null) || (this.idSetor != null && !this.idSetor.equals(other.idSetor)));
    }

    @Override
    public String toString() {
        return "alocar.java.models.Setorusuario[ idSetor=" + idSetor + " ]";
    }
    
}
