/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.util.Objects;

/**
 *
 * @author lucas.leite
 */
public class Reserva {
    private Integer id_reserva;
    private Integer id_veiculo;
    private Integer id_cliente;
    private Integer id_status;

    public Integer getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(Integer id_reserva) {
        this.id_reserva = id_reserva;
    }

    public Integer getId_veiculo() {
        return id_veiculo;
    }

    public void setId_veiculo(Integer id_veiculo) {
        this.id_veiculo = id_veiculo;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_status() {
        return id_status;
    }

    public void setId_status(Integer id_status) {
        this.id_status = id_status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_reserva != null ? id_reserva.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reserva other = (Reserva) obj;
        return Objects.equals(this.id_reserva, other.id_reserva);
    }

    @Override
    public String toString() {
        return "Reserva{" + "id_reserva=" + id_reserva + ", id_veiculo=" + id_veiculo + ", id_cliente=" + id_cliente + ", id_status=" + id_status + '}';
    }

    
    

    
    
}
