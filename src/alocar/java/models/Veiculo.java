/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas.leite
 */
@Entity
@Table(name = "veiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Veiculo.findAll", query = "SELECT v FROM Veiculo v")
    , @NamedQuery(name = "Veiculo.findById", query = "SELECT v FROM Veiculo v WHERE v.id_veiculo = :id_veiculo")
    , @NamedQuery(name = "Veiculo.findByChassi", query = "SELECT v FROM Veiculo v WHERE v.chassi = :chassi")
    , @NamedQuery(name = "Veiculo.findByMarca", query = "SELECT v FROM Veiculo v WHERE v.marca = :marca")
    , @NamedQuery(name = "Veiculo.findByModelo", query = "SELECT v FROM Veiculo v WHERE v.modelo = :modelo")
    , @NamedQuery(name = "Veiculo.findByAno", query = "SELECT v FROM Veiculo v WHERE v.ano = :ano")
    , @NamedQuery(name = "Veiculo.findByTipo", query = "SELECT v FROM Veiculo v WHERE v.tipo = :tipo")
    , @NamedQuery(name = "Veiculo.findByTransmissao", query = "SELECT v FROM Veiculo v WHERE v.transmissao = :transmissao")
    , @NamedQuery(name = "Veiculo.findByCor", query = "SELECT v FROM Veiculo v WHERE v.cor = :cor")
    , @NamedQuery(name = "Veiculo.findByCombustivel", query = "SELECT v FROM Veiculo v WHERE v.combustivel = :combustivel")
    , @NamedQuery(name = "Veiculo.findByPortas", query = "SELECT v FROM Veiculo v WHERE v.portas = :portas")
    , @NamedQuery(name = "Veiculo.findByStatusV", query = "SELECT v FROM Veiculo v WHERE v.status_veiculo = :status_veiculo")})
public class Veiculo implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_veiculo")
    private Integer id_veiculo;
    @Basic(optional = false)
    @Column(name = "chassi")
    private String chassi;
    @Basic(optional = false)
    @Column(name = "marca")
    private String marca;
    @Basic(optional = false)
    @Column(name = "modelo")
    private String modelo;
    @Basic(optional = false)
    @Column(name = "ano")
    private String ano;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;
    @Basic(optional = false)
    @Column(name = "transmissao")
    private String transmissao;
    @Basic(optional = false)
    @Column(name = "cor")
    private String cor;
    @Basic(optional = false)
    @Column(name = "combustivel")
    private String combustivel;
    @Basic(optional = false)
    @Column(name = "portas")
    private String portas;
    @Basic(optional = false)
    @Column(name = "status_veiculo")
    private Integer statusVeiculo;
    public Veiculo() {
    }

    public Veiculo(String chassi) {
        this.chassi = chassi;
    }

    public Veiculo(Integer id_veiculo, String chassi, String marca, String modelo, String ano, String tipo, String transmissao, String cor, String combustivel, String portas) {
        this.id_veiculo = id_veiculo;
        this.chassi = chassi;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.tipo = tipo;
        this.transmissao = transmissao;
        this.cor = cor;
        this.combustivel = combustivel;
        this.portas = portas;
    }

    public Veiculo(Integer id, String marca, String modelo, String chassi) {
        this.id_veiculo = id;
        this.marca = marca;
        this.modelo = modelo;
        this.chassi = chassi;
        
    }

    public Integer getId_veiculo() {
        return id_veiculo;
    }

    public void setId_veiculo(Integer id_veiculo) {
        this.id_veiculo = id_veiculo;
    }
    
    public String getChassi() {
        return chassi;
    }

    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTransmissao() {
        return transmissao;
    }

    public void setTransmissao(String transmissao) {
        this.transmissao = transmissao;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public String getPortas() {
        return portas;
    }

    public void setPortas(String portas) {
        this.portas = portas;
    }
    public Integer getStatusVeiculo(){
        return statusVeiculo;
    }
    public void setStatusVeiculo(Integer statusVeiculo){
        this.statusVeiculo = statusVeiculo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chassi != null ? chassi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Veiculo)) {
            return false;
        }
        Veiculo other = (Veiculo) object;
        return !((this.chassi == null && other.chassi != null) || (this.chassi != null && !this.chassi.equals(other.chassi)));
    }

    @Override
    public String toString() {
        return "alocar.java.models.Veiculo[ chassi=" + chassi + " ]";
    }

    public String getMarcaEModelo() {
       return marca+" - "+modelo + " - " +chassi;
    }

}
