/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lucas Soares
 */
@Entity
@Table(name = "statusreserva")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statusreserva.findAll", query = "SELECT s FROM Statusreserva s")
    , @NamedQuery(name = "Statusreserva.findByIdStatusr", query = "SELECT s FROM Statusreserva s WHERE s.idStatusr = :idStatusr")
    , @NamedQuery(name = "Statusreserva.findByStatus", query = "SELECT s FROM Statusreserva s WHERE s.status = :status")})
public class Statusreserva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_statusr")
    private Integer idStatusr;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;

    public Statusreserva() {
    }

    public Statusreserva(Integer idStatusr) {
        this.idStatusr = idStatusr;
    }

    public Statusreserva(Integer idStatusr, String status) {
        this.idStatusr = idStatusr;
        this.status = status;
    }

    public Integer getIdStatusr() {
        return idStatusr;
    }

    public void setIdStatusr(Integer idStatusr) {
        this.idStatusr = idStatusr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatusr != null ? idStatusr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statusreserva)) {
            return false;
        }
        Statusreserva other = (Statusreserva) object;
        if ((this.idStatusr == null && other.idStatusr != null) || (this.idStatusr != null && !this.idStatusr.equals(other.idStatusr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alocar.java.models.Statusreserva[ idStatusr=" + idStatusr + " ]";
    }
    
}
