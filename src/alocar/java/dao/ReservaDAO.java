/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.Reserva;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author lucas.leite
 */
public class ReservaDAO {
    public void Inserir(Reserva reserva) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "INSERT INTO alocar_java.reservas (veiculo,cliente,status) VALUES (?,?,?)";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, reserva.getId_veiculo());
            stmt.setInt(2, reserva.getId_cliente());
            stmt.setInt(3, reserva.getId_status());
            stmt.execute();
            System.out.println("Inseriu!");
        }
        stmt.close();
    }
}
