/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.Statusreserva;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Lucas Soares
 */
public class StatusReservaDAO {
    public List<Statusreserva> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List statusRes;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            if(!(arg.isEmpty()) && !" ".equals(arg)){
                rs = stmt.executeQuery("SELECT * FROM alocar_java.statusreserva WHERE id_statusr like &"+arg+"% OR  status like '%"+arg+"%'");    
            } else{
                rs = stmt.executeQuery("SELECT * FROM alocar_java.statusreserva");
            }
            statusRes = new ArrayList();
            while (rs.next()){
                statusRes.add(rs);
            }
        }
        return statusRes;
    }
    
    public Object popularStatusReserva(ResultSet rs) throws SQLException{
        Statusreserva str = new Statusreserva();
        str.setIdStatusr(rs.getInt("id_statusr"));
        str.setStatus(rs.getString("status"));
        return str;
    }
    
    public HashMap<String, Integer> popularComboBox() throws SQLException{
        HashMap <String, Integer> map = new HashMap<>();
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        Statement st = con.createStatement();
        ResultSet rs;
        Statusreserva str;
        rs = st.executeQuery("SELECT * FROM alocar_java.statusreserva");
        while(rs.next()){
            str = new Statusreserva(rs.getInt("id_statusr"), rs.getString("statis"));
            map.put(str.getStatus(), str.getIdStatusr());
        }
        //TODO oncreateReserva Default value will be 2 - AVALIAÇÂO
        
        return map;
    }
    
}
