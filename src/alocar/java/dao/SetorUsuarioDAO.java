/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.SetorUsuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas.leite
 */
public class SetorUsuarioDAO  {
    public void Inserir(SetorUsuario setuser) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "INSERT INTO alocar_java.setorusuario (nome_setor) VALUES (?)";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, setuser.getNomeSetor());
            stmt.execute();
        }
        stmt.close();
    }
    
    public void Alterar(SetorUsuario setuser) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "UPDATE alocar_java.setorusuario SET (nome_setor = ?) ";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, setuser.getNomeSetor());
            stmt.execute();
        }
        stmt.close();
    }
    public void Deletar(Integer id) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "DELETE FROM alocar_java.setorusuario WHERE chassi = ?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
        }
        stmt.close();
    }
    
    public List<SetorUsuario> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List setorusers;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            if(!(arg.isEmpty()) && !" ".equals(arg)){
                rs = stmt.executeQuery("SELECT * FROM alocar_java.setorusuario WHERE nome_setor like %"+arg+"%");
            } else{
                rs = stmt.executeQuery("SELECT * FROM alocar_java.setorusuario");
            }
            setorusers = new ArrayList();
            while (rs.next()){
                setorusers.add(populaSetores(rs));
            }
        }
        return setorusers;
    }

    private Object populaSetores(ResultSet rs) throws SQLException {
        SetorUsuario setuser = new SetorUsuario();
        setuser.setIdSetor(rs.getInt("id_setor"));
        setuser.setNomeSetor(rs.getString("nome_setor"));
        
        return setuser;
    }
}
