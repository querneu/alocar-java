/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.Cliente;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas.leite
 */
public class ClienteDAO {
    public void Inserir(Cliente cliente) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "INSERT INTO alocar_java.cliente (cpf,nome_completo,data_nascimento,email,cnh,cnpj,nome_fantasia) VALUES (?,?,?,?,?,?,?)";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, cliente.getCpf());
            stmt.setString(2, cliente.getNomeCompleto());
            stmt.setString(3, cliente.getDataNascimento());
            stmt.setString(4, cliente.getEmail());
            stmt.setString(5, cliente.getCnh());
            stmt.setString(6, cliente.getCnpj());
            stmt.setString(7, cliente.getNomeFantasia());
            stmt.execute();
        }
        stmt.close();
    }
    
    public void Alterar(Cliente cliente) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "UPDATE alocar_java.cliente SET (nome_completo = ?, data_nascimento = ?, email = ?, cnh = ?, cnpj = ?, nome_fantasia = ?) where cpf = ?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, cliente.getNomeCompleto());
            stmt.setString(2, cliente.getDataNascimento());
            stmt.setString(3, cliente.getEmail());
            stmt.setString(4, cliente.getCnh());
            stmt.setString(5, cliente.getCnpj());
            stmt.setString(6, cliente.getNomeFantasia());
            stmt.setString(7, cliente.getCpf());
            stmt.execute();
        }
        stmt.close();
    }
    
    public void Deletar(Integer id) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "DELETE FROM alocar_java.cliente WHERE id_cliente = ?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
        }
        stmt.close();
    }
    
    public List<Cliente> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List clientes;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            if(!(arg.isEmpty()) && !" ".equals(arg)){
                rs = stmt.executeQuery("SELECT * FROM alocar_java.cliente WHERE cpf like %"+arg+"% OR nome_completo like %"+arg+"% OR email like %"+arg+"% or cnh like %"+arg+"%");
            } else{
                rs = stmt.executeQuery("SELECT * FROM alocar_java.cliente");
            }
            clientes = new ArrayList();
            while (rs.next()){
                clientes.add(populaClientes(rs));
            }
        }
        return clientes;
    }

    private Object populaClientes(ResultSet rs) throws SQLException {
        Cliente cliente = new Cliente();
        cliente.setIdCliente(rs.getInt("id_cliente"));
        cliente.setCpf(rs.getString("cpf"));
        cliente.setNomeCompleto(rs.getString("nome_completo"));
        cliente.setDataNascimento(rs.getString("data_nascimento"));
        cliente.setEmail(rs.getString("email"));
        cliente.setCnh(rs.getString("cnh"));
        cliente.setCnpj(rs.getString("cnpj"));
        cliente.setNomeFantasia(rs.getString("nome_fantasia"));
        return cliente;
    }

   
}
