/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.Statusveiculo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author lucas.leite
 */
public class StatusVeiculoDAO {
    public List<Statusveiculo> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List status;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            if(!(arg.isEmpty()) && !" ".equals(arg)){
                rs = stmt.executeQuery("SELECT * FROM alocar_java.statusveiculo WHERE id_statusv like &"+arg+"% OR  status like '%"+arg+"%'");    
            } else{
                rs = stmt.executeQuery("SELECT * FROM alocar_java.statusveiculo");
            }
            status = new ArrayList();
            while (rs.next()){
                status.add(rs);
            }
        }
        return status;
    }
    
    public Object popularStatusVeiculo(ResultSet rs)throws SQLException{
        Statusveiculo status = new Statusveiculo();
        status.setIdStatusv(rs.getInt("id_statusv"));
        status.setStatus(rs.getString("status"));
        return status;
    }
    
    public HashMap<String, Integer> popularComboBox() throws SQLException{
        HashMap<String, Integer> map = new HashMap<>();
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        Statement st = con.createStatement();
        ResultSet rs;
        Statusveiculo stv;
        rs = st.executeQuery("SELECT * FROM alocar_java.statusveiculo");
        while(rs.next()){
            stv = new Statusveiculo(rs.getInt("id_statusv"), rs.getString("status"));
            map.put(stv.getStatus(), stv.getIdStatusv());
        }
        return map;
    }
}   
    
