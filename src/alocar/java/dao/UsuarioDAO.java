/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.Usuario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas.leite
 */
public class UsuarioDAO {
     public void Inserir(Usuario usuario) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "INSERT INTO alocar_java.usuario (nome_completo,login,senha,email,setor,perfil) VALUES (?,?,?,?,?,?)";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getNomeCompleto());
            stmt.setString(2, usuario.getLogin());
            stmt.setString(3, usuario.getSenha());
            stmt.setString(4, usuario.getEmail());
            stmt.setInt(5, usuario.getSetor());
            stmt.setInt(6, usuario.getPerfil());
            stmt.execute();
        }
        stmt.close();
    }
     
    public void Alterar(Usuario usuario) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "UPDATE alocar_java.usuario SET (nome_completo = ?,login = ?,senha = ?,email = ?,setor = ?,perfil = ?) where id_usuario = ?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, usuario.getIdUsuario());
            stmt.setString(2, usuario.getNomeCompleto());
            stmt.setString(3, usuario.getLogin());
            stmt.setString(4, usuario.getSenha());
            stmt.setString(5, usuario.getEmail());
            stmt.setInt(6, usuario.getSetor());
            stmt.setInt(7, usuario.getPerfil());
            stmt.execute();
        }
        stmt.close();
    }
    
    public void Deletar(Integer id) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "DELETE FROM alocar_java.usuario WHERE id = ?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
        }
        stmt.close();
    }
    
    
    public List<Usuario> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List usuarios;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            if(!(arg.isEmpty()) && !" ".equals(arg)){
                rs = stmt.executeQuery("SELECT * FROM alocar_java.usuario WHERE id_usuario like %"+arg+"% or nome_completo like %"+arg+"% or login like %"+arg+"% or email like %"+arg+"% ");
            } else{
                rs = stmt.executeQuery("SELECT * FROM alocar_java.usuario");
            }
            usuarios = new ArrayList();
            while (rs.next()){
                usuarios.add(popularUsuarios(rs));
            }
        }
        return usuarios;
    }
   
           
    public List<Usuario> Logar (String login, String senha) throws SQLException{
        ResultSet rs = null;
        List usuario;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            
            if(!(login.isEmpty()) && !" ".equals(login) && !(senha.isEmpty()) && !" ".equals(senha)){
                 String sql = "SELECT * FROM alocar_java.usuario WHERE login = '"+login+"' and senha = '"+senha+"'";
                rs = stmt.executeQuery(sql);
            } 
            usuario = new ArrayList();
            while (rs.next()){//Acontece...
                usuario.add(popularUsuarios(rs));
            }
            stmt.close();
        }
        System.out.println("Encontrado: "+usuario.size());
        return usuario;
    }
    
    private Object popularUsuarios(ResultSet rs)throws SQLException {
         Usuario usuario = new Usuario();
         usuario.setIdUsuario(rs.getInt("id_usuario"));
         usuario.setNomeCompleto(rs.getString("nome_completo"));
         usuario.setLogin(rs.getString("login"));
         usuario.setSenha(rs.getString("senha"));
         usuario.setEmail(rs.getString("email"));
         usuario.setSetor(rs.getInt("setor"));
         usuario.setPerfil(rs.getInt("perfil"));
         return usuario;
    }
    
}
