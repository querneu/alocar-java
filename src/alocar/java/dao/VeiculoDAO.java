/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;
import alocar.java.models.Veiculo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
/**
 *
 * @author lucas.leite
 */
public class VeiculoDAO {
    public void Inserir(Veiculo veiculo) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "INSERT INTO alocar_java.veiculo (chassi,marca,modelo,ano,tipo,transmissao,cor,combustivel,portas, status_veiculo) VALUES (?,?,?,?,?,?,?,?,?,?)";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, veiculo.getChassi());
            stmt.setString(2, veiculo.getMarca());
            stmt.setString(3, veiculo.getModelo());
            stmt.setString(4, veiculo.getAno());
            stmt.setString(5, veiculo.getTipo());
            stmt.setString(6, veiculo.getTransmissao());
            stmt.setString(7, veiculo.getCor());
            stmt.setString(8, veiculo.getCombustivel());
            stmt.setString(9, veiculo.getPortas());
            stmt.setInt(10, Integer.parseInt(veiculo.getStatusVeiculo().toString()));
            stmt.execute();
        }
        stmt.close();
    }
    public void Alterar(Veiculo veiculo) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "UPDATE alocar_java.veiculo SET (marca = ?,modelo = ?,ano = ?,tipo = ?,transmissao = ?,cor = ?,combustivel = ?,portas = ?, status_veiculo = ?) WHERE chassi = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, veiculo.getMarca());
            stmt.setString(2, veiculo.getModelo());
            stmt.setString(3, veiculo.getAno());
            stmt.setString(4, veiculo.getTipo());
            stmt.setString(5, veiculo.getTransmissao());
            stmt.setString(6, veiculo.getCor());
            stmt.setString(7, veiculo.getCombustivel());
            stmt.setString(8, veiculo.getPortas());
            stmt.setInt(9, veiculo.getStatusVeiculo());
            stmt.setString(10, veiculo.getChassi()); //conversar com o grupo e tentar convencer sobre o uso de um ID na tabela (id não pode ser alterado :))
            stmt.execute();
        }
        stmt.close();
    }
    public void Deletar(String id) throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "DELETE FROM alocar_java.veiculo WHERE chassi = ?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, id);
            stmt.execute();
        }
        stmt.close();
    }
    public List<Veiculo> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List veiculos;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
            if(!(arg.isEmpty()) && !" ".equals(arg)){
                rs = stmt.executeQuery("SELECT * FROM alocar_java.veiculo WHERE chassi like '%"+arg+"%' or marca like '%"+arg+"%' or modelo like '%"+arg+"%' or cor like '%"+arg+"%'");
            } else{
                rs = stmt.executeQuery("SELECT * FROM alocar_java.veiculo");
            }
            veiculos = new ArrayList();
            while (rs.next()){
                veiculos.add(popularVeiculos(rs));
            }
        }
        return veiculos;
    }
    public LinkedHashMap<String, Integer> popularComboBox() throws SQLException{
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        Statement st = con.createStatement();
        ResultSet rs;
        Veiculo stv;
        rs = st.executeQuery("SELECT * FROM alocar_java.veiculo ORDER BY id_veiculo ASC");
        while(rs.next()){
            stv = new Veiculo(rs.getInt("id_veiculo"), rs.getString("marca"), rs.getString("modelo"), rs.getString("chassi"));
            map.put(stv.getMarcaEModelo(), stv.getId_veiculo());
        }
        return map;
    }
    private Object popularVeiculos(ResultSet rs) throws SQLException {
        Veiculo veiculo = new Veiculo();
        veiculo.setId_veiculo(rs.getInt("id_veiculo"));
        veiculo.setChassi(rs.getString("chassi"));
        veiculo.setMarca(rs.getString("marca"));
        veiculo.setModelo(rs.getString("modelo"));
        veiculo.setAno(rs.getString("ano"));
        veiculo.setTipo(rs.getString("tipo"));
        veiculo.setTransmissao(rs.getString("transmissao"));
        veiculo.setCor(rs.getString("cor"));
        veiculo.setCombustivel(rs.getString("combustivel"));
        veiculo.setPortas(rs.getString("portas"));
        veiculo.setStatusVeiculo(rs.getInt("status_veiculo"));
        return veiculo;
    }
}