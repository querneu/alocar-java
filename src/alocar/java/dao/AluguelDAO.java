/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocar.java.dao;

import alocar.java.models.Aluguel;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Soares
 */
public class AluguelDAO {
    public void Inserir(Aluguel aluguel)throws SQLException{
        PreparedStatement stmt;
        try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","")){
            String sql = "INSERT INTO alocar_java.aluguel (id_reserva,dt_inicio,dt_fim) VALUES (?,?,?)";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, aluguel.getIdReserva());
            stmt.setDate(2, (Date) aluguel.getDtInicio());
            stmt.setDate(3, (Date) aluguel.getDtFim());
            stmt.execute();
        }
    }
    public List<Aluguel> Buscar (String arg) throws SQLException{
        ResultSet rs;
        List alugueis;
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alocar_java?useSSL=false","root","");
        try(Statement stmt = con.createStatement()){
        rs = stmt.executeQuery("SELECT * FROM alocar_java.aluguel");
        }
        alugueis = new ArrayList();
        while(rs.next()){
            alugueis.add(populaAlugueis(rs));
        }
        return alugueis;
    
    }

    private Object populaAlugueis(ResultSet rs) throws SQLException{
        Aluguel aluguel = new Aluguel();
        aluguel.setIdAluguel(rs.getInt("id_aluguel"));
        aluguel.setIdReserva(rs.getInt("id_reserva"));
        aluguel.setDtInicio(rs.getDate("dt_inicio"));
        aluguel.setDtInicio(rs.getDate("dt_fim"));
        return aluguel;
    }
}
